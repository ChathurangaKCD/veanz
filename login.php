<?php
/**
 * Created by PhpStorm.
 * User: ChathurangaKCD
 * Date: 5/11/2016
 * Time: 12:04 PM
 */
session_start();
require("bz/UserHandler.php");
$dashboard_url = "index.php";
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    require_once("html/templates/login.html");
} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['login_username']) && isset($_POST['login_password'])) {
        $result = login($_POST['login_username'], $_POST['login_password']);
        $_SESSION['id'] = $result['id'];
        $_SESSION['name'] = $result['name'];
        $_SESSION['isAdmin'] = $result['admin_status'];
        header("Location: $dashboard_url");
    } else {

    }
}