<?php
	/**
	 * Object represents table 'management'
	 *
     	 * @author: http://phpdao.com
     	 * @date: 2016-05-03 07:04	 
	 */
	class Management{
		
		var $id;
		var $name;
		var $username;
		var $password;
		var $contactNo;
		var $adminStatus;
		var $nic;
		var $email;
		
	}
?>