<?php
	/**
	 * Object represents table 'drivers'
	 *
     	 * @author: http://phpdao.com
     	 * @date: 2016-05-03 07:04	 
	 */
	class Driver{
		
		var $id;
		var $name;
		var $username;
		var $password;
		var $contactNo;
		var $nic;
		var $email;
		var $licenseNo;
		
	}
?>