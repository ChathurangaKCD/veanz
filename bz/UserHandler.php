<?php
/**
 * Created by PhpStorm.
 * User: ChathurangaKCD
 * Date: 5/11/2016
 * Time: 1:48 PM
 */
function login($usr, $pwd)
{
    $link = connect();
    $query = "SELECT id,name,admin_status from management WHERE username=? and password=? ";
    if (($stmt = $link->prepare($query))) {
        if (!$stmt->bind_param("ss", $usr, $pwd)) {
            error("2. Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);
        }
        if (!$stmt->execute()) {
            error("3. Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        } else {
            if (!$stmt->bind_result($id, $name,$is_admin)) {
                error("4. Execute failed: (" . $stmt->errno . ") " . $stmt->error);
            }
            if (!$stmt->fetch()) {
                error("5. Execute failed: (" . $stmt->errno . ") " . $stmt->error);
            }
            return array('id' => $id, 'name' => $name,'admin_status'=>$is_admin);
        }
    } else {
        error("1. Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
    }
}
function connect()
{
    $link = mysqli_connect("127.0.0.1", "root", "", "fuel_usg");

    if (!$link) {
        echo "Error: Unable to connect to MySQL." . PHP_EOL;
        echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
        echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }

//echo "Success: A proper connection to MySQL was made! The my_db database is great." . PHP_EOL;
//echo "Host information: " . mysqli_get_host_info($link) . PHP_EOL;

    return $link;
}

function error($error){
    $message = array('id' => -1, 'name' => '','status'=>$error);
    header('Content-type:application/json;charset=utf-8');
    echo json_encode($message);
}