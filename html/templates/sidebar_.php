<?php

/**
 * Created by PhpStorm.
 * User: ChathurangaKCD
 * Date: 5/11/2016
 * Time: 3:53 PM
 */
class Component
{
    private $link;
    private $text;
    private $class;
    private $length;
    private $subC;

    function __construct($link, $text, $length)
    {
        $this->link = $link;
        $this->text = $text;
        $this->length = $length;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return Component[]
     */
    public function getSubC()
    {
        return $this->subC;
    }

    /**
     * @param mixed $subC
     */
    public function setSubC($subC)
    {
        $this->subC = $subC;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }
}


$l21=new Component("panels.html", "L21", 0);
$l1=new Component("panels.html", "L1", 0);
$l2=new Component("panels.html", "L2", 1);
$l2->setSubC(array($l21));
$l1->setClass("fa fa-sitemap");
$l2->setClass("fa fa-edit");
$sidebar = array($l1,$l2);
printSideBar($sidebar);

/**
 * @param Component[] $arr
 */
function printSideBar($arr)
{
    foreach ($arr as $comp) {
        echo '<li><a href="';
        echo $comp->getLink();
        echo '"><i class="';
        echo $comp->getClass();
        echo '"></i>';
        echo $comp->getText();
        echo '</a>';
        if ($comp->getLength() != 0) {
            echo "<ul>";
            foreach ($comp->getSubC() as $sub) {
                echo '<li><a href="';
                echo $sub->getLink();
                echo '">';
                echo $sub->getText();
                echo "</a></li>";
            }
            echo "</ul>";
        }
        echo "</li>";
    }
}
