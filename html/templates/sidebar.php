<nav class="ts-sidebar">
    <ul class="ts-sidebar-menu">
        <li class="ts-label">Search</li>
        <li>
            <input type="text" class="ts-sidebar-search" placeholder="Search here...">
        </li>
        <li class="ts-label">Main</li>
        <li class="open"><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <?php require("sidebar_.php"); ?>
        <li><a href="#"><i class="fa fa-desktop"></i> UI Elements</a>
            <ul>
                <li><a href="panels.html">Panels and Wells</a></li>
                <li><a href="buttons.html">Buttons</a></li>
                <li><a href="notifications.html">Notifications</a></li>
                <li><a href="typography.html">Typography</a></li>
                <li><a href="icon.html">Icon</a></li>
                <li><a href="grid.html">Grid</a></li>
            </ul>
        </li>
        <li><a href="tables.html"><i class="fa fa-table"></i> Tables</a></li>
        <li><a href="forms.html"><i class="fa fa-edit"></i> Forms</a></li>
        <li><a href="charts.html"><i class="fa fa-pie-chart"></i> Charts</a></li>
        <li><a href="#"><i class="fa fa-sitemap"></i> Multi-Level Dropdown</a>
            <ul>
                <li><a href="#">2nd level</a></li>
                <li><a href="#">2nd level</a></li>
                <li><a href="#">3rd level</a>
                    <ul>
                        <li><a href="#">3rd level</a></li>
                        <li><a href="#">3rd level</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a href="#"><i class="fa fa-files-o"></i> Sample Pages</a>
            <ul>
                <li><a href="blank.html">Blank page</a></li>
                <li><a href="login.html">Login page</a></li>
            </ul>
        </li>

    </ul>
</nav>

<div class="content-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">

                <h2 class="page-title"><?php echo $title ?></h2>